<?php
/**
 * @file
 * Contains the module constants.
 */

define("ENTIY_FLATSTORE_VARIABLE_PREFIX", "entity_flatstore_store_");
define("ENTIY_FLATSTORE_VARIABLE_SEPARATOR", "__");
define("ENTIY_FLATSTORE_ACTIVE_STORES_TABLE_NAME", "entity_flatstore_active_stores");