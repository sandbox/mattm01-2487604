<?php
/**
 * @file
 * Contains the permissions hook to setup module specific permissions.
 */

/**
 * Implements hook_permission().
 */
function entity_flatstore_permission() {
  return array(
    'administer entity flatstore' => array(
      'title' => t('Administer Entity Flatstore'),
      'description' => t('Perform administration tasks for Entity Flatstore.'),
    ),
  );
}