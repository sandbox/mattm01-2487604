<?php
/**
 * @file
 * Contains the module specific functions (helper functions, etc.).
 */

/**
 * Helper function to build the machine name
 */
function _entity_flatstore_build_store_machine_name($entity_name, $bundle_name) {
  $machine_name = ENTIY_FLATSTORE_VARIABLE_PREFIX.
                  $entity_name.
                  ENTIY_FLATSTORE_VARIABLE_SEPARATOR.
                  $bundle_name;
  
  return $machine_name;
}

/**
 * Helper function to extract the entity and bundle from string
 */
function _entity_flatstore_extract_entity_bundle_from_string($entity_bundle_string) {
  $entity_bundle = str_replace(ENTIY_FLATSTORE_VARIABLE_PREFIX, '', $entity_bundle_string);
  $entity_bundle_array = explode(ENTIY_FLATSTORE_VARIABLE_SEPARATOR, $entity_bundle);
  
  return array(
    'entity_name' => $entity_bundle_array[0],
    'bundle_name' => $entity_bundle_array[1]
  );
}

/**
 * Helper function to get the availble bundles
 */
function _entity_flatstore_get_availible_bundles() {
  $availble_bundles = array();
  $bundles = field_info_bundles();

  foreach ($bundles as $entity_name => $entity) :
    // skip entities that don't have any bundles
    if (count($entity) < 1) :
      continue;
    endif;

    $availble_bundles[$entity_name] = array();

    foreach ($entity as $bundle_name => $bundle) :
      $store_name = _entity_flatstore_build_store_machine_name($entity_name, $bundle_name);
      $store_enabled = _entity_flatstore_check_store_enabled($store_name);

      // Skip enabled stores since they are managed on the other page.
      if ($store_enabled) :
        continue;
      endif;

      $availble_bundles[$entity_name][$bundle_name] = array(
        'store_name' => $store_name,
        'entity_name' => $entity_name,
        'bundle_name' => $bundle_name,
        'bundle_label' => $bundle['label']
      );
    endforeach;

    // unset any field sets that don't have any availble stores
    if (count($availble_bundles[$entity_name]) <= 0) :
      unset($availble_bundles[$entity_name]);
    endif;
    
  endforeach;

  return $availble_bundles;
}

/**
 * Helper function to get the enabled flatstores
 */
function _entity_flatstore_get_enabled_stores() {
  $query = db_select(ENTIY_FLATSTORE_ACTIVE_STORES_TABLE_NAME, 's')
              ->fields('s', array('machine_name'))
              ->execute()
              ->fetchCol();
  
  return $query;
}

/**
 * Helper function to check if a store is enabled
 * It just checks if there is a matching record
 */
function _entity_flatstore_check_store_enabled($store_machine_name) {
  $enabled_stores_query = db_select(ENTIY_FLATSTORE_ACTIVE_STORES_TABLE_NAME, 's')
                            ->fields('s')
                            ->condition('machine_name', $store_machine_name, '=')
                            ->range(0, 1)
                            ->execute();

  $num_active_stores = $enabled_stores_query->rowCount();

  return (int) $num_active_stores > 0;
}

/**
 * Helper function to enable a store
 */
function _entity_flatstore_enable_store($table_name) {
  $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($table_name);

  $entity_name = $entity_bundle_array['entity_name'];
  $bundle_name = $entity_bundle_array['bundle_name'];

  $schema = _entity_flatstore_build_store_schema($entity_name, $bundle_name);

  $store_table = data_create_table($table_name, $schema, $table_name);

  _entity_flatstore_track_store($table_name);
}

/**
 * Helper function to build a store schema
 */
function _entity_flatstore_build_store_schema($entity_name, $bundle_name) {
  $store_name = _entity_flatstore_build_store_machine_name($entity_name, $bundle_name);

  $entity_info = entity_get_info($entity_name);

  // short hand variables
  $entity_id = $entity_info['entity keys']['id'];
  
  $entity_label = FALSE;
  if (isset($entity_info['entity keys']['label'])) :
    $entity_label = $entity_info['entity keys']['label'];
  endif;

  $base_table_fields = $entity_info['schema_fields_sql']['base table'];
  $bundle_fields = field_info_instances($entity_name, $bundle_name);
  
  $schema = array(
    'description' => "Entity flatstore table for {$store_name}",
    'fields' => array(),

    // @TODO add indexes
    // created / changed etc.?
    'indexes' => array(
      //'id' => array($entity_id),
      //'node_created' => array('created'),
    ),

    /*
    'unique keys' => array(
      'nid_vid' => array('nid', 'vid'),
      'vid' => array('vid'),
    ),
    */
    'primary key' => array($entity_id),
  );

  // add base entity fields (id, label)
  // @TODO un hard code?
  $schema['fields'][$entity_id] = array(
    'description' => 'The id.',
    'type' => 'serial',
    'unsigned' => TRUE,
    'not null' => TRUE
  );

  if (isset($entity_label)) :
    $schema['fields'][$entity_label] = array(
      'description' => 'The label.',
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => ''
    );
  endif;

  // add base table fields to the schema
  // these are the entity fields outside of the field api
  
  foreach ($base_table_fields as $key => $base_table_field) :
    // skip ID and label fields since they are in the entity keys
    if ( $base_table_field === $entity_id ||
         ($entity_label && $base_table_field === $entity_label)):
      continue;
    endif;

    // @TODO don't hardcode these
    // use api function to get SQL column config
    $schema['fields'][$base_table_field] = array(
      'description' => $base_table_field,
      'type' => 'varchar',
      'length' => 255
    );
  endforeach;

  // add field api fields
  foreach ($bundle_fields as $field_name => $field) :
    $field_info = field_info_field($field_name);

    foreach ($field_info['columns'] as $column_name => $column_value) :
      $schema['fields'][$field_name.ENTIY_FLATSTORE_VARIABLE_SEPARATOR.$column_name] = $column_value;
    endforeach;
  endforeach;

  return $schema;
}

/**
 * Helper function to track store
 * Adds a record to the table that tracks enabled stores
 */
function _entity_flatstore_track_store($machine_name) {
  $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($machine_name);

  $id = db_insert(ENTIY_FLATSTORE_ACTIVE_STORES_TABLE_NAME)
          ->fields(array(
              'machine_name' => $machine_name,
              'entity_name' => $entity_bundle_array['entity_name'],
              'bundle_name' => $entity_bundle_array['bundle_name'],
            ))
          ->execute();

  return $id;
}

/**
 * Helper function to rebuild a store's contents using batch api.
 */
function _entity_flatstore_rebuild_store_batch($store_name) {
  $store_table = data_get_table($store_name);

  if ($store_table) :
    $store_table->handler()->truncate();
  endif;

  $operations = array();

  $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($store_name);
  
  // shorthand variables
  $entity_name = $entity_bundle_array['entity_name'];
  $bundle_name = $entity_bundle_array['bundle_name'];

  $entity_query = new EntityFieldQuery();

  $entity_query->entityCondition('entity_type', $entity_name)
                ->addMetaData('account', user_load(1));

  // see https://www.drupal.org/node/1343708
  // bundle. (not supported in comment entity types)
  if ($entity_name !== 'comment') :
    $entity_query->entityCondition('bundle', $bundle_name);
  endif;

  $entity_result = $entity_query->execute();

  if (isset($entity_result[$entity_name])) :
    foreach ($entity_result[$entity_name] as $entity_id => $entity_info) :
       $operations[] = array(
          'entity_flatstore_ui_batch_insert_store_record',
          array($store_name, $entity_name, $entity_id)
        );
    endforeach;
  endif;

  batch_set(array(
    'title' => t('Storing Entities ...'),
    'operations' => $operations,
    'init_message' => t('Starting ...'),
    'error_message' => t('An error occurred'),
    'finished' => 'entity_flatstore_ui_batch_insert_store_record_finished',
    'file' => drupal_get_path('module', 'entity_flatstore_ui') . '/includes/entity_flatstore_ui.batch.inc'
  ));

  batch_process('');
}

/**
 * Helper function to rebuild a store's contents.
 */
function _entity_flatstore_rebuild_store($store_name) {
  $store_table = data_get_table($store_name);

  if ($store_table) :
    $store_table->handler()->truncate();
  endif;

  $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($store_name);
  
  // shorthand variables
  $entity_name = $entity_bundle_array['entity_name'];
  $bundle_name = $entity_bundle_array['bundle_name'];

  $entity_query = new EntityFieldQuery();

  $entity_query->entityCondition('entity_type', $entity_name)
                ->addMetaData('account', user_load(1));

  // see https://www.drupal.org/node/1343708
  // bundle. (not supported in comment entity types)
  if ($entity_name !== 'comment') :
    $entity_query->entityCondition('bundle', $bundle_name);
  endif;

  $entity_result = $entity_query->execute();

  if (isset($entity_result[$entity_name])) :
    foreach ($entity_result[$entity_name] as $entity_id => $entity_info) :
      _entity_flatstore_insert_store_record($store_name, $entity_name, $entity_id);
    endforeach;
  endif;
}

/**
 * Helper function to clear a store's contents.
 */
function _entity_flatstore_clear_store($table_name) {
  $store_table = data_get_table($table_name);

  if ($store_table) :
    $store_table->handler()->truncate();
  endif;
}

/**
 * Helper function to delete a store.
 */
function _entity_flatstore_delete_store($table_name) {
  $store_table = data_get_table($table_name);

  if ($store_table) :
    $store_table->drop();
  endif;

  _entity_flatstore_untrack_store($table_name);
}

/**
 * Helper function to untrack store
 * Removes a record to the table that tracks enabled stores
 */
function _entity_flatstore_untrack_store($machine_name) {
  db_delete(ENTIY_FLATSTORE_ACTIVE_STORES_TABLE_NAME)
    ->condition('machine_name', $machine_name)
    ->execute();
}

/**
 * Helper function to get the record count of a store
 */
function _entity_flatstore_get_store_count($machine_name) {
  $store_query = db_select($machine_name, 'fst')
                        ->fields('fst')
                        ->execute();

  $store_count = $store_query->rowCount();

  return $store_count;
}

/**
 * Helper function to get the total number of entities that a store is supposed to track
 */
function _entity_flatstore_get_store_entity_count($machine_name) {
  $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($machine_name);
  $entity_name = $entity_bundle_array['entity_name'];
  $bundle_name = $entity_bundle_array['bundle_name'];

  $entity_query = new EntityFieldQuery();

  $entity_query->entityCondition('entity_type', $entity_name)
                  ->addMetaData('account', user_load(1));

  // see https://www.drupal.org/node/1343708
  if ($entity_name !== 'comment') :
    $entity_query->entityCondition('bundle', $bundle_name);
  endif;

  $entity_result = $entity_query->execute();

  $entity_count = 0;

  if (isset($entity_result[$entity_name])) :
    $entity_count = count($entity_result[$entity_name]);
  endif;

  return $entity_count;
}

/**
 * Helper function to insert a store record
 * Used for batch operations
 */
function _entity_flatstore_insert_store_record($table_name, $entity_name, $entity_id) {
  $store_table = data_get_table($table_name);

  if (!$store_table) :
    return;
  endif;

  $store_record = array();    
  $entity = entity_load($entity_name, array($entity_id));
  $entity = reset($entity);

  $store_record = _entity_flatstore_build_record($table_name, $entity);

  $store_table->handler()->insert($store_record);
}

/**
 * Helper function to keep entity hook code dry.
 */
function _entity_flatstore_node_type_delete_callback($info) {
  $entity_name = 'node';
  $bundle_name = $info->type;

  $table_name = _entity_flatstore_build_store_machine_name($entity_name, $bundle_name);
  _entity_flatstore_delete_store($table_name);
}

/**
 * Helper function to keep entity hook code dry.
 */
function _entity_flatstore_entity_insert_update_callback($entity, $type, $update = FALSE) {
  // @TODO un hardcode this
  // only save published versions

  $save_unpublished = variable_get('entity_flatstore_save_unpublished', 0);

  if (!$save_unpublished && (isset($entity->status) && !$entity->status)) :
    return;
  endif;

  $entity_name = $type;
  $bundle_name = '';

  if (isset($entity->type)) :
    $bundle_name = $entity->type;
  endif;

  $table_name = _entity_flatstore_build_store_machine_name($entity_name, $bundle_name);

  $store_table = data_get_table($table_name);

  if (!$store_table) :
    return;
  endif;
  
  $store_record = _entity_flatstore_build_record($table_name, $entity);

  if ($update) :
    $table_schema = drupal_get_schema($table_name);
    $id_field = reset($table_schema['primary key']);
    
    // update record or
    // insert if, forwhatever reason, the record isn't in the flat table
    $store_record_check = db_select($table_name, 'fst')
      ->fields('fst', array($id_field))
      ->condition($id_field, $entity->{$id_field}, '=')
      ->range(0, 1)
      ->execute()
      ->rowCount();

    if ($store_record_check) :
      $store_table->handler()->update($store_record, $id_field);
    else :
      $store_table->handler()->insert($store_record);
    endif;
  else :
    $store_table->handler()->insert($store_record);
  endif;
}

/**
 * Helper function to keep entity hook code in same place.
 */
function _entity_flatstore_entity_delete_callback($entity, $type) {
  // Would lead to logically complex code to throw this in 
  // _entity_flatstore_entity_insert_update()
  // hence it's not abstracted

  $entity_name = $type;
  $bundle_name = '';

  if (isset($entity->type)) :
    $bundle_name = $entity->type;
  endif;

  $table_name = _entity_flatstore_build_store_machine_name($entity_name, $bundle_name);

  $store_table = data_get_table($table_name);

  if (!$store_table) :
    return;
  endif;

  $table_schema = drupal_get_schema($table_name);
  $id_field = reset($table_schema['primary key']);
  $store_table->handler()->delete(array($id_field => $entity->{$id_field}));
}

/**
 * Helper function to keep field instance hook code dry.
 */
function _entity_flatstore_field_create_update_delete_instance_callback($instance, $update = FALSE, $delete = FALSE) {
  $table_name = _entity_flatstore_build_store_machine_name($instance['entity_type'], $instance['bundle']);

  $store_table = data_get_table($table_name);

  if (!$store_table) :
    return;
  endif;

  $field_info = field_info_field($instance['field_name']);
  
  foreach ($field_info['columns'] as $column_name => $column_value) :
    if ($update) :
      $store_table->changeField($instance['field_name'].ENTIY_FLATSTORE_VARIABLE_SEPARATOR.$column_name, $column_value);
    elseif ($delete) :
      $store_table->dropField($instance['field_name'].ENTIY_FLATSTORE_VARIABLE_SEPARATOR.$column_name);
    else :
      $store_table->addField($instance['field_name'].ENTIY_FLATSTORE_VARIABLE_SEPARATOR.$column_name, $column_value);
    endif;
  endforeach;
}

/**
 * Helper function to build out a flatstore record.
 */
function _entity_flatstore_build_record($table_name, $entity) {
  $table_schema = drupal_get_schema($table_name);

  foreach ($table_schema['fields'] as $field_name => $field_config) :      
    // @TODO clean this up - don't hardcode lookup for field api fields
    $field_array = explode(ENTIY_FLATSTORE_VARIABLE_SEPARATOR, $field_name);
    
    $field_base_name = $field_array[0];
    $field_sub_name = FALSE;

    if (isset($field_array[1])) :
      $field_sub_name = $field_array[1];
    endif;

    if (!isset($entity->{$field_base_name})) :
      continue;
    endif;

    // @TODO make this more robust
    // the assumption here is that if subfield name is set
    // it must be a field api field - not sure if that is
    // always the case
    if ($field_sub_name &&!empty($entity->{$field_base_name})) :
      if (isset($entity->{$field_base_name}[LANGUAGE_NONE]) &&
          !empty($entity->{$field_base_name}[LANGUAGE_NONE]) &&
          isset($entity->{$field_base_name}[LANGUAGE_NONE][0][$field_sub_name]) &&
          !empty($entity->{$field_base_name}[LANGUAGE_NONE][0][$field_sub_name])) :
        $entity_field_value = $entity->{$field_base_name}[LANGUAGE_NONE][0][$field_sub_name];
      else:
        // @TODO cleaner way to do this?
        // when set to '' or NULL it saves as 'Array'
        $entity_field_value = NULL;
      endif;
    else:
      $entity_field_value = $entity->{$field_base_name};
    endif;

    $store_record[$field_name] = $entity_field_value;
  endforeach;

  return $store_record;
}
