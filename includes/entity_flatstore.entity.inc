<?php
/**
 * @file
 * Contains entity hooks (insert, delete, etc).
 */

/**
 * Implements hook_node_type_delete.
 * 
 * Remove the store if the content type is deleted.
 * @todo way to do this for entites?
 */
function entity_flatstore_node_type_delete($info) {
  _entity_flatstore_node_type_delete_callback($info);
}

/**
 * Implements hook_entity_insert().
 */
function entity_flatstore_entity_insert($entity, $type) {
  _entity_flatstore_entity_insert_update_callback($entity, $type);
}

/**
 * Implements hook_entity_update().
 */
function entity_flatstore_entity_update($entity, $type) {
  _entity_flatstore_entity_insert_update_callback($entity, $type, TRUE);
}

/**
 * Implements hook_entity_delete().
 */
function entity_flatstore_entity_delete($entity, $type) {
  _entity_flatstore_entity_delete_callback($entity, $type);
}

/**
 * Implements hook_field_create_instance().
 */
function entity_flatstore_field_create_instance($instance) {
  _entity_flatstore_field_create_update_delete_instance_callback($instance);
}

/**
 * Implements hook_field_update_instance().
 */
function entity_flatstore_field_update_instance($instance) {
  _entity_flatstore_field_create_update_delete_instance_callback($instance, TRUE);
}

/**
 * Implements hook_field_delete_instance().
 */
function entity_flatstore_field_delete_instance($instance) {
  _entity_flatstore_field_create_update_delete_instance_callback($instance, FALSE, TRUE);
}