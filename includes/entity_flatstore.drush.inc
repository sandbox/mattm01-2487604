<?php
/**
 * @file
 * Contains the drush integration code.
 */

/**
 * Implements hook_drush_command().
 */
function entity_flatstore_drush_command() {

  $items['entity-flatstore-option-unpublished'] = array(
    'description' => 'Set if flatstores should save unpublished entities.',
    'aliases' => array('efs-o-u'),
    'arguments' => array(
      'yn' => 'Y or n (if flatstores should save unpublished entities).',
    ),
  );

  $items['entity-flatstore-list-availible'] = array(
    'description' => 'List all availible entity stores.',
    'aliases' => array('efs-la'),
  );

  $items['entity-flatstore-list-enabled'] = array(
    'description' => 'List all enabled entity stores.',
    'aliases' => array('efs-le'),
  );

  $items['entity-flatstore-store-stats'] = array(
    'description' => 'List the current stats for an enabled store.',
    'aliases' => array('efs-ss'),
    'arguments' => array(
      'store' => 'The machine name of the store to enable.',
    ),
  );

  $items['entity-flatstore-enable'] = array(
    'description' => 'Enable a specific entity store.',
    'aliases' => array('efs-en'),
    'arguments' => array(
      'store' => 'The machine name of the store to enable.',
    ),
  );

  $items['entity-flatstore-rebuild'] = array(
    'description' => 'Rebuild the contents (restore) of a specific entity store.',
    'aliases' => array('efs-rb'),
    'arguments' => array(
      'store' => 'The machine name of the store to enable.',
    ),
  );

  $items['entity-flatstore-clear'] = array(
    'description' => 'Clear the contents of a specific entity store.',
    'aliases' => array('efs-cl'),
    'arguments' => array(
      'store' => 'The machine name of the store to enable.',
    ),
  );

  $items['entity-flatstore-delete'] = array(
    'description' => 'Delete a specific entity store.',
    'aliases' => array('efs-del'),
    'arguments' => array(
      'store' => 'The machine name of the store to enable.',
    ),
  );

  return $items;
}

/**
 * Callback for the entity-flatstore-option-unpublished command
 */
function drush_entity_flatstore_option_unpublished($yn) {
  $variable_name = 'entity_flatstore_store_unpublished';

  if ($yn === 'y') :
    variable_set($variable_name, TRUE);

    drush_log("Flatstores will now store unpublished entities.", 'ok');
  elseif ($yn === 'n') :
    variable_set($variable_name, FALSE);

    drush_log("Flatstores will now NOT store unpublished entities.", 'ok');
  else : 
    drush_log("Please pass y or n as the argument.", 'warning');
  endif;
}

/**
 * Callback for the entity-flatstore-list-availible command
 */
function drush_entity_flatstore_list_availible() {
  $availible_stores = _entity_flatstore_get_availible_bundles();

  $output_table = array(
    array(
      'Entity',
      'Bundle',
      'Store Machine Name'
    )
  );

  foreach ($availible_stores as $entity_name => $entity) :
    foreach ($entity as $bundle_name => $bundle) :
      $output_table[] = array(
        $entity_name,
        $bundle_name,
        _entity_flatstore_build_store_machine_name($entity_name, $bundle_name)
      );
    endforeach;
  endforeach;

  drush_print_table($output_table, TRUE);
}

/**
 * Callback for the entity-flatstore-list-enabled command
 */
function drush_entity_flatstore_list_enabled() {
  $enabled_stores = _entity_flatstore_get_enabled_stores();

  $output_table = array(
    array(
      'Entity',
      'Bundle',
      'Store Machine Name',
      'Stored Entities',
      'Total Entities'
    )
  );

  foreach ($enabled_stores as $key => $store_name) :
    $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($store_name);

    $entity_name = $entity_bundle_array['entity_name'];
    $bundle_name = $entity_bundle_array['bundle_name'];

    $store_count = _entity_flatstore_get_store_count($store_name);
    $entity_count = _entity_flatstore_get_store_entity_count($store_name);

    $output_table[] = array(
      $entity_name,
      $bundle_name,
      $store_name,
      $store_count,
      $entity_count
    );
  endforeach;

  drush_print_table($output_table, TRUE);
}

/**
 * Callback for the entity-flatstore-store-stats command
 */
function drush_entity_flatstore_store_stats($store_name) {

  $output_table = array(
    array(
      'Entity',
      'Bundle',
      'Store Machine Name',
      'Stored Entities',
      'Total Entities'
    )
  );

  $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($store_name);

  $entity_name = $entity_bundle_array['entity_name'];
  $bundle_name = $entity_bundle_array['bundle_name'];

  $store_count = _entity_flatstore_get_store_count($store_name);
  $entity_count = _entity_flatstore_get_store_entity_count($store_name);

  $output_table[] = array(
    $entity_name,
    $bundle_name,
    $store_name,
    $store_count,
    $entity_count
  );

  drush_print_table($output_table, TRUE);
}

/**
 * Callback for the entity-flatstore-enable command
 */
function drush_entity_flatstore_enable($store_name) {
  _entity_flatstore_enable_store($store_name);

  drush_log("{$store_name} store enabled.", 'ok');
}

/**
 * Callback for the entity-flatstore-rebuild command
 */
function drush_entity_flatstore_rebuild($store_name) {
  _entity_flatstore_rebuild_store($store_name);

  drush_log("{$store_name} store rebuilt.", 'ok');
}

/**
 * Callback for the entity-flatstore-clear command
 */
function drush_entity_flatstore_clear($store_name) {
  _entity_flatstore_clear_store($store_name);

  drush_log("{$store_name} store cleared.", 'ok');
}

/**
 * Callback for the entity-flatstore-delete command
 */
function drush_entity_flatstore_delete($store_name) {
  _entity_flatstore_delete_store($store_name);

  drush_log("{$store_name} store deleted.", 'ok');
}