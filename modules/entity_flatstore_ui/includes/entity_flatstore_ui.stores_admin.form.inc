<?php
/**
 * @file
 * Contains the callback fucntion that builds the page.
 */

/**
 * Page callback.
 */
function entity_flatstore_ui_stores_admin_form($form, &$form_state) {
  $enabled_stores = _entity_flatstore_get_enabled_stores();

  $form = array();

  $form['intro'] = array(
    '#markup' => '<p>Table Stats.</p>'
  );

  foreach ($enabled_stores as $key => $enabled_store) :
    $entity_bundle_array = _entity_flatstore_extract_entity_bundle_from_string($enabled_store);

    // shorthand variables
    $entity_name = $entity_bundle_array['entity_name'];
    $bundle_name = $entity_bundle_array['bundle_name'];
    //$bundle_label = $form_state['bundles'][$entity_name][$bundle_name]['label'];

    // create the fieldset if it dosen't exist
    // if statment used incase multiple bundles are active for a given entity
    // Dont want to overwrite the fieldset - it would only have the last bundle
    if (!isset($form[$entity_name])) :
      $form[$entity_name] = array(
        '#type' => 'fieldset',
        '#title' => t($entity_name),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
    endif;


    $store_count = _entity_flatstore_get_store_count($enabled_store);
    $entity_count = _entity_flatstore_get_store_entity_count($enabled_store);
  
    $form[$entity_name][$enabled_store] = array(
      // @TODO cleaner way to do the markup? Use FORM API?
      '#markup' => "<div><strong>{$enabled_store}</strong>".
                      "<p>{$store_count} of {$entity_count} records stored.</p>".
                      "<p>".
                        "<a class='button' href='/".ENTIY_FLATSTORE_ADMIN_URL."/store-operation/rebuild/{$enabled_store}'>(Re)Store all records</a>".
                        "<a class='button' href='/".ENTIY_FLATSTORE_ADMIN_URL."/store-operation/clear/{$enabled_store}'>Clear all records</a>".
                        "<a class='button' href='/".ENTIY_FLATSTORE_ADMIN_URL."/store-operation/delete/{$enabled_store}'>Delete store</a>".
                      "</p>".
                    "</div>"
    );
  endforeach;

  return $form;
}
