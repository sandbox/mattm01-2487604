<?php 
/**
 * @file
 * Contains the callback function that builds the admin form.
 */

/**
 * Page callback.
 */
function entity_flatstore_ui_configuration_form($form, $form_state) {
  $form = array(
    'entity_flatstore_store_unpublished' => array(
      '#type' => 'checkbox',
      '#title' => t('Store unpublished entities?'),
      '#default_value' => variable_get('entity_flatstore_store_unpublished', 0),
      '#description' => t("Check this box if you want to store unpublished entities. Note that the status field is availble, so even if you store unpublished entities, you can query to only get publihsed entities. E.g. status = 1 is published; status = 0 is unpublished."),
    ),
  );

  return system_settings_form($form);
}
