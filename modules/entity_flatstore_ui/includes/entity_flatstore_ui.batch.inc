<?php
/**
 * @file
 * Contains the batch functions.
 */

/**
 * Creates the entity flatstore tables
 *
 * Runs as a batch operation
 */
function entity_flatstore_ui_batch_enable_store($table_name, &$context) {
  _entity_flatstore_enable_store($table_name);

  $context['results'][] = $table_name;
  $context['message'] = t('Enabling store "@table_name"', array('@table_name' => $table_name));
}

/**
 * Runs after the create table batch operations
 */
function entity_flatstore_ui_batch_enable_store_finished($success, $results, $operations) {
  if ($success) :
    drupal_set_message(t('@count store(s) enabled.', array('@count' => count($results))));
  else :
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  endif;
}

/**
 * Drops the entity flatstore tables
 *
 * Runs as a batch operation
 */
function entity_flatstore_ui_batch_delete_store($table_name, &$context) {
  _entity_flatstore_delete_store($table_name);

  $context['results'][] = $table_name;
  $context['message'] = t('Deleting store "@table_name"', array('@table_name' => $table_name));
}

/**
 * Runs after the drop table batch operations
 */
function entity_flatstore_ui_batch_delete_store_finished($success, $results, $operations) {
  if ($success) :
    drupal_set_message(t('@count store(s) deleted.', array('@count' => count($results))));
  else :
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  endif;
}

/**
 * Inserts the entity into the flatstore table
 *
 * Runs as a batch operation
 */
function entity_flatstore_ui_batch_insert_store_record($table_name, $entity_name, $entity_id, &$context) {
  _entity_flatstore_insert_store_record($table_name, $entity_name, $entity_id);

  $context['results'][] = $entity_id;
  $context['message'] = t('Inserting entity with ID: "@entity_id"', array('@entity_id' => $entity_id));
}

/**
 * Runs after the drop table batch operations
 */
function entity_flatstore_ui_batch_insert_store_record_finished($success, $results, $operations) {
  if ($success) :
    drupal_set_message(t('@count entity(s) inserted.', array('@count' => count($results))));
  else :
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  endif;

  drupal_goto(ENTIY_FLATSTORE_ADMIN_URL);
}