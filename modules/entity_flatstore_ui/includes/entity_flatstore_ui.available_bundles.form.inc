<?php
/**
 * @file
 * Contains the callback fucntion that builds the admin form.
 */

/**
 * Page callback.
 */
function entity_flatstore_ui_available_bundles_settings_form($form, &$form_state) {
  $form = array();

  $form_state['bundles'] = field_info_bundles();
  $form_state['available_bundles'] = _entity_flatstore_get_availible_bundles();

  $form['intro'] = array(
    '#markup' => '<p>Below is a list of available bundles grouped by entity. Check the box next to the bundle to store those entites (and hit save). <strong>Enabled bundles are excluded because they are managed on the Enabled Bundles page</strong>.</p>'
  );

  foreach ($form_state['available_bundles'] as $entity_name => $entity) :

    $form[$entity_name] = array(
      '#type' => 'fieldset',
      '#title' => t($entity_name),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    foreach ($entity as $bundle_name => $bundle) :
      $form[$entity_name][$bundle['store_name']] = array(
        '#type' => 'checkbox',
        '#title' => t("Store {$bundle['bundle_label']} entities ({$entity_name}: {$bundle_name})?"),
      );
    endforeach;
  endforeach;

  $form['submit'] = array( 
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Callback function that saves form data
 */
function entity_flatstore_ui_available_bundles_settings_form_submit($form, &$form_state) {
  $operations = array();

  foreach ($form_state['values'] as $value_name => $value) :

    // Skip check boxes that don't start with Entity Flatstore prefix or that aren't checked
    if(substr($value_name, 0, strlen(ENTIY_FLATSTORE_VARIABLE_PREFIX)) !== ENTIY_FLATSTORE_VARIABLE_PREFIX || !$value) :
      continue;
    endif;

    foreach ($form_state['available_bundles'] as $entity_name => $entity) :
      foreach ($entity as $bundle_name => $bundle) :

        $store_name = $bundle['store_name'];

        if ($store_name !== $value_name) :
          continue;
        endif;

        $store_table = data_get_table($store_name);

        // Skip if table already exists for whatever reason.
        if ($store_table) :
          continue;
        endif;

        $operations[] = array(
          'entity_flatstore_ui_batch_enable_store',
          array($store_name)
        );

      endforeach;
    endforeach;

  endforeach;

  batch_set(array(
    'title' => t('Enabling Stores ...'),
    'operations' => $operations,
    'init_message' => t('Starting ...'),
    'error_message' => t('An error occurred'),
    'finished' => 'entity_flatstore_ui_batch_enable_store_finished',
    'file' => drupal_get_path('module', 'entity_flatstore_ui') . '/includes/entity_flatstore_ui.batch.inc'
  ));
}
