<?php
/**
 * @file
 * Contains the hook menu - routing stuff.
 */

/**
 * Implements hook_menu().
 */
function entity_flatstore_ui_menu() {
  $items[ENTIY_FLATSTORE_ADMIN_URL] = array(
    'title' => 'Entity Flatstore',
    'description' => 'Entity Flatstore Enabled Stores Page.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_flatstore_ui_stores_admin_form'),
    'file' => 'includes/entity_flatstore_ui.stores_admin.form.inc',
    'access callback' => 'user_access',
    'access arguments' => array('administer entity flatstore'),
    'theme callback' => 'variable_get',
    'theme arguments' => array('admin_theme'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 9,
  );

   $items[ENTIY_FLATSTORE_ADMIN_URL.'/enabled'] = array(
    'title' => 'Enabled Stores',
    'weight' => 10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items[ENTIY_FLATSTORE_ADMIN_URL.'/available'] = array(
    'title' => 'Available Stores',
    'description' => 'Entity Flatstore Available Stores Configuration Page.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_flatstore_ui_available_bundles_settings_form'),
    'file' => 'includes/entity_flatstore_ui.available_bundles.form.inc',
    'access callback' => 'user_access',
    'access arguments' => array('administer entity flatstore'),
    'theme callback' => 'variable_get',
    'theme arguments' => array('admin_theme'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 11,
  );

  $items[ENTIY_FLATSTORE_ADMIN_URL.'/store-operation/rebuild/%'] = array(
    'title' => 'Table Stats',
    'description' => 'Entity Flatstore Stores Stats Page Rebuild.',
    'page callback' => 'entity_flatstore_ui_stores_stats_rebuild',
    'page arguments' => array(5),
    'file' => 'includes/entity_flatstore_ui.stores_stats.page.inc',
    'access callback' => 'user_access',
    'access arguments' => array('administer entity flatstore'),
    'theme callback' => 'variable_get',
    'theme arguments' => array('admin_theme'),
    'type' => MENU_CALLBACK,
  );

  $items[ENTIY_FLATSTORE_ADMIN_URL.'/store-operation/clear/%'] = array(
    'title' => 'Table Stats',
    'description' => 'Entity Flatstore Stores Stats Page Clear.',
    'page callback' => 'entity_flatstore_ui_stores_stats_clear',
    'page arguments' => array(5),
    'file' => 'includes/entity_flatstore_ui.stores_stats.page.inc',
    'access callback' => 'user_access',
    'access arguments' => array('administer entity flatstore'),
    'theme callback' => 'variable_get',
    'theme arguments' => array('admin_theme'),
    'type' => MENU_CALLBACK,
  );

  $items[ENTIY_FLATSTORE_ADMIN_URL.'/store-operation/delete/%'] = array(
    'title' => 'Table Stats',
    'description' => 'Entity Flatstore Stores Stats Page Delete.',
    'page callback' => 'entity_flatstore_ui_stores_stats_delete',
    'page arguments' => array(5),
    'file' => 'includes/entity_flatstore_ui.stores_stats.page.inc',
    'access callback' => 'user_access',
    'access arguments' => array('administer entity flatstore'),
    'theme callback' => 'variable_get',
    'theme arguments' => array('admin_theme'),
    'type' => MENU_CALLBACK,
  );

  $items[ENTIY_FLATSTORE_ADMIN_URL.'/configuration'] = array(
    'title' => 'Configuration',
    'description' => 'Entity Flatstore Configuration Page.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_flatstore_ui_configuration_form'),
    'file' => 'includes/entity_flatstore_ui.configuration.form.inc',
    'access callback' => 'user_access',
    'access arguments' => array('administer entity flatstore'),
    'theme callback' => 'variable_get',
    'theme arguments' => array('admin_theme'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 13,
  );

  return $items;
}