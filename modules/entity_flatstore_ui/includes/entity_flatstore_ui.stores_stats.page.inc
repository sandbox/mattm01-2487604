<?php
/**
 * @file
 * Contains the callback fucntion that builds the page.
 */

/**
 * (Re)Store all records.
 */
function entity_flatstore_ui_stores_stats_rebuild($store_name) {
  $store_table = data_get_table($store_name);

  if ($store_table) :
    _entity_flatstore_rebuild_store_batch($store_name);
  endif;
}

/**
 * Clear all records.
 */
function entity_flatstore_ui_stores_stats_clear($store_name) {
  $store_table = data_get_table($store_name);

  if ($store_table) :
    _entity_flatstore_clear_store($store_name);

    drupal_set_message(t("{$store_name} store records cleared."));
  else :
    drupal_set_message(t("{$store_name} store not found."), 'warning');
  endif;

  drupal_goto(ENTIY_FLATSTORE_ADMIN_URL);
}

/**
 * Delete the store
 */
function entity_flatstore_ui_stores_stats_delete($store_name) {
  $store_table = data_get_table($store_name);

  if ($store_table) :
    _entity_flatstore_delete_store($store_name);

    drupal_set_message(t("{$store_name} store deleted."));
  else :
    drupal_set_message(t("{$store_name} store not found."), 'warning');
  endif;

  drupal_goto(ENTIY_FLATSTORE_ADMIN_URL);
}
