<?php
/**
 * @file
 * Contains the callback fucntion that builds the page.
 */

/**
 * Config page
 */
function entity_flatstore_example_config_page() {
  return <<<MARKUP
  <p>The below endpoints will return all of the Article Nodes as JSON (<strong>be cautious if you have alot of articles</strong>). The Flatdata versions should be significantly faster for large data sets.</p>
  <p><strong>Make sure you have an article content type and that the article flatstore is enabled and populated. See the <a href="/admin/structure/entity-flatstore">stores admin page (requires Entity Flatstore UI to be enabled).</a></strong></p>
  <h4>Article Node JSON</h4>
  <ul>
    <li><a href="/entity-flatstore-example/article-node-json">All articles returned as JSON using Drupal core (node load).</a></li>
    <li><a href="/entity-flatstore-example/article-flatstore-json"><strong>All articles returned as JSON using Entity flatstore.</strong></a></li>
  </ul>

  <h4>Article Node JSON using Views / Views Datasource</h4>
  <p>Note: Views only loads NID, Title, created, updated and body value.</p>
  <ul>
    <li><a href="/entity-flatstore-example/article-node-view-json">All articles returned as JSON using views and views data source json.</li>
    <li><a href="/entity-flatstore-example/article-flatstore-view-json"><strong>All articles returned as JSON using Entity flatstore view (built with Data module) and views data source json.</strong></a></li>
  </ul>
MARKUP;
}
