<?php
/**
 * @file
 * Contains the callback fucntion that builds the page.
 */

/**
 * Article nodes json resonse using built in Drupal code
 */
function entity_flatstore_example_article_node_json() {
  // could use Entity query, but that is alot of overhead for this
  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'article')
    ->execute()
    ->fetchCol(); // returns an indexed array
  
  $nodes = array();
  $loaded_nodes = node_load_multiple($nids);

  foreach ($loaded_nodes as $key => $node) :
    $nodes[] = array('node' => $node);
  endforeach;

  return array('nodes' => $nodes);
}

/**
 * Article from flatstore json resonse using entity flatstore
 */
function entity_flatstore_example_article_flatstore_json() {
  $nodes_query = db_select('entity_flatstore_store_node__article', 'a')
    ->fields('a')
    ->orderBy('created', 'DESC')
    ->execute();

  $nodes = array();

  while ($node = $nodes_query->fetchAssoc()) :
    $nodes[] = array('node' => $node);
  endwhile;

  return array('nodes' => $nodes);
}